<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('order_id')->nullable();
            $table->uuid('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->uuid('contest_id');
            $table->foreign('contest_id')
                ->references('id')->on('contests')
                ->onDelete('cascade');
            $table->text('transition_id')->nullable();
            $table->text('bank_txn_id')->nullable();
            $table->text('amount');
            $table->text('tnx_reason')->nullable();
            $table->timestamp('tnx_date')->nullable();
            $table->text('get_way')->nullable();
            $table->text('bank_name')->nullable();
            $table->text('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
