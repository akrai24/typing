<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypingTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('typing_tests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('contest_id');
            $table->uuid('user_id');
            $table->bigInteger('wpm');
            $table->bigInteger('word_count');
            $table->bigInteger('errors');
            $table->bigInteger('points');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('contest_id')->references('id')->on('contests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('typing_tests');
    }
}
