<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrizingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prizings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('contest_id');
            $table->integer('rank_from');
            $table->integer('rank_to');
            $table->integer('amount');
            $table->timestamps();
            $table->foreign('contest_id')->references('id')->on('contests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prizings');
    }
}
