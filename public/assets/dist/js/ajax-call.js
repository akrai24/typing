$(function () {
    $('#leader-board').on('show.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var id = button.data('id');
        var url = button.data('url');
        var modal = $(this);
        var request = $.ajax({
            url: url,
            type: "GET",
            dataType: "html"
        });
        request.done(function (data) {
            modal.find('.loader').removeClass('d-flex').addClass('d-none');
            modal.find('.modal-body').append(data);
        });
        request.fail(function (jqXHR, textStatus) {
            console.log("Request Failed: "+textStatus);
        });
    });
    $('#leader-board').on('hide.bs.modal', function (e) {
        var modal = $(this);
        modal.find('.contest-details').remove();
        modal.find('.loader').removeClass('d-none').addClass('d-flex');
    })
});