@extends('layouts.users')

@section('page-title', $title)

@section('content')

    {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'patch','class' => 'py-5' ]) !!}

    <div class="row">

        <div class="col-sm-6">
            <h5 class="section-title mb-3">User Details</h5>
            <div class="form-group row">
                    {!! Form::label('name', 'Name :', ['class' => 'col-sm-3 col-form-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('name', null, ['class' => 'form-control '.(($errors->has("name")) ? 'is-invalid' : ''),'placeholder' => 'Enter Name']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('email', 'Email :', ['class' => 'col-sm-3 col-form-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('email', null, ['class' => 'form-control '.(($errors->has("email")) ? 'is-invalid' : ''),'placeholder' => 'Enter Email', 'readonly'=> true]) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('mobile', 'Mobile :', ['class' => 'col-sm-3 col-form-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('mobile', null, ['class' => 'form-control '.(($errors->has("mobile")) ? 'is-invalid' : ''),'placeholder' => 'Enter Mobile', 'readonly'=> true]) !!}
                </div>
            </div>

        </div>

        <div class="col-sm-6">

            <h5 class="section-title mb-3">Accounts Details</h5>
            <div class="form-group row">
                {!! Form::label('account_number', 'A/C Number :', ['class' => 'col-sm-3 col-form-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('account_number', null, ['class' => 'form-control '.(($errors->has("account_number")) ? 'is-invalid' : ''),'placeholder' => 'Enter A/C Number']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('ifsc', 'IFSC :', ['class' => 'col-sm-3 col-form-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('ifsc', null, ['class' => 'form-control '.(($errors->has("ifsc")) ? 'is-invalid' : ''),'placeholder' => 'Enter IFSC']) !!}
                </div>
            </div>

        </div>

        <div class="col-12 mt-2 mb-5 text-center">
            {!! Form::submit('Update', ['class' => 'btn btn-success px-5']) !!}
        </div>
    </div>


    {!! Form::close() !!}

@endsection