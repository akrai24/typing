@extends('layouts.users')

@section('page-title', $title)

@section('content')
    <ul class="nav nav-pills mb-3 nav-fill" id="contestsTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all"
               aria-selected="true">All Contests</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="running-tab" data-toggle="tab" href="#running" role="tab"
               aria-controls="running" aria-selected="false">Running Contests</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="expired-tab" data-toggle="tab" href="#expired" role="tab"
               aria-controls="expired" aria-selected="false">Expired Contests</a>
        </li>
    </ul>
    <div class="tab-content" id="contests">
        <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
            <div class="row">
                @foreach ($contests as $contest)
                    @include('contest.contest-grid')
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade" id="running" role="tabpanel" aria-labelledby="running-tab">
            <div class="row">
                @foreach ($contestActive as $contest)
                    @include('contest.contest-grid')
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade" id="expired" role="tabpanel" aria-labelledby="contact-tab">
            <div class="row">
                @foreach ($expired as $contest)
                    @include('contest.contest-grid')
                @endforeach
            </div>
        </div>
    </div>
    @include('contest._leader-board-modal')
@endsection