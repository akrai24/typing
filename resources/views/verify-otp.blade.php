@extends('layouts.login')

@section('content')
    <div class="card">
    <div class="card-body register-card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
      <p class="login-box-msg">Verify Otp</p>

      <form method="POST" action="{{ route('phoneverification.verify') }}">
          @csrf
        <div class="form-group has-feedback">
            <input id="name" type="text" placeholder="OTP" class="form-control @error('otp') is-invalid @enderror" name="otp" value="{{ old('otp') }}" required autofocus>
            @if ($errors->has('otp'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('otp') }}</strong>
                </span>
            @endif
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('Verify') }}</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
@endsection
