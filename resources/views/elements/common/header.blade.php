<div class="header">
    <div class="d-flex justify-content-between">
        <div class="Dream11">
            <!-- <img src="images/logo.png" width="200px" height="40px"> -->
            <h3 class="logo-text">
                <a href="{{ route('home') }}">Typing</a>
            </h3>
        </div>

        <div class="login">
            <ul class="nav">
                <li class="nav-item {{ Route::is('welcome') ? 'active' : '' }}">
                    <a href="{{route('welcome')}}" class="nav-link">Home</a>
                </li>
                <li class="nav-item {{ Route::is('about-Us') ? 'active' : '' }}">
                    <a href="{{route('about-Us')}}" class="nav-link">About</a>
                </li>
                <li class="nav-item {{ Route::is('contact-Us') ? 'active' : '' }}">
                    <a href="{{route('contact-Us')}}" class="nav-link">Contact</a>
                </li>
                <li class="nav-item  {{ Route::is('faq') ? 'active' : '' }}">
                    <a href="{{route('faq')}}" class="nav-link">Faq</a>
                </li>
                @auth()
                    <li class="nav-item dropdown">
                        <a href="#;" class="nav-link dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name  }}</a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{ route('user.show', Auth::user()->id) }}" class="dropdown-item">My Profile</a>
                            <a href="{{ route('myContests')  }}" class="dropdown-item">My Contests</a>
                            <a href="{!! route('user.earning', Auth::user()->id) !!}" class="dropdown-item">My Earning</a>
                            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                        </div>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">{{ csrf_field() }}</form>
                @endauth
                @guest
                    <li class="nav-item"><a href="{{route("login")}}" class="nav-link btn btn-primary btn-sm">LOG IN</a></li>
                @endguest
            </ul>
        </div>
    </div>
</div>