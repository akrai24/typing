<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title><?php echo (isset($title)) ? $title : "Fantacy Typing"; ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">

</head>
<body>

    <div class="container-fluid">
        @include('elements.common.header')
    </div>

    <section class="page-head">
        <div class="page-heading py-5">
            <h3 class="page-title text-center">@yield('page-title')</h3>
        </div>
    </section>

    <div class="container">
        <div class="my-4">
            @include('common.flash-message')
        </div>
        @yield('content')
    </div>

    @include('elements.common.footer')

    <script src="{{asset('assets/dist/js/time.js')}}"></script>
    <script src="{!! asset('assets/dist/js/ajax-call.js') !!}"></script>
    <script>
        $(".DateCountdown").TimeCircles({
            use_background: false,
            circle_bg_color: "#000000",
            time: {
                Days: { color: "#ffffff" },
                Hours: { color: "#ffffff" },
                Minutes: { color: "#ffffff" },
                Seconds: { color: "#ffffff" }
            }
        });
    </script>
</body>
</html>
