<!DOCTYPE html>
<html>
<head>
  <title><?php echo (isset($title)) ? $title : "Fantacy Typing"; ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">

</head>
<body>
  <div class="container-fluid">
    @include('elements.common.header')
    <div class="banner row justify-content-end">
      <div class="full-bg slider">
      </div>
      <div class="banner-text">
        <div class="">
          <p class="total-contest">Total <span class="text-danger">{{$contests->count()}}</span> Contest are active right now.</p>
          <h1 class="">India Largest Fantasy Typing Site</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="my-4">
      @include('common.flash-message')
    </div>
    @yield('content')
  </div>
  <section class="ftco-section ftco-counter info-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-4 d-flex justify-content-center counter-wrap ftco-animate fadeInUp ftco-animated">
              <div class="block-18 text-center">
                <div class="text">
                  <div class="icon">
                    <i class="fas fa-keyboard"></i>
                  </div>
                  <strong class="number">{{$contestCount}}</strong>
                  <span>Total Contest</span>
                </div>
              </div>
            </div>
            <div class="col-md-4 d-flex justify-content-center counter-wrap ftco-animate fadeInUp ftco-animated">
              <div class="block-18 text-center">
                <div class="text">
                  <div class="icon">
                    <i class="fas fa-satellite"></i>
                  </div>
                  <strong class="number">{{$contestActive}}</strong>
                  <span>Running Contest</span>
                </div>
              </div>
            </div>
            <div class="col-md-4 d-flex justify-content-center counter-wrap ftco-animate fadeInUp ftco-animated">
              <div class="block-18 text-center">
                <div class="text">
                  <div class="icon">
                    <i class="fas fa-users"></i>
                  </div>
                  <strong class="number">{{$usersCount}}</strong>
                  <span>Total Users</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section ftco-counter testimonials">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate fadeInUp ftco-animated">
              <div class="block-18 text-center">
                <div class="text">
                  <div class="icon">
                  <svg version="1.1" viewBox="0 0 250 250" xmlns="http://www.w3.org/2000/svg">
                  <g transform="translate(-266.43 -401.65)">
                    <g transform="matrix(.68333 0 0 .68333 581.94 550.77)" fill="#95bbdf">
                    <rect x="-461.73" y="-218.23" width="365.85" height="365.85" rx="0" ry="0" color="#000000" fill="#95bbdf"/>
                    </g>
                    <g transform="matrix(.3492 0 0 .3492 284.57 419.79)" fill="#FFF">
                    <g fill="#FFF">
                      <path d="m306 325.99c90.56-0.01 123.15-90.68 131.68-165.17 10.51-91.76-32.88-160.82-131.68-160.82-98.78 0-142.19 69.055-131.68 160.82 8.54 74.484 41.114 165.18 131.68 165.17z"/>
                      <path d="m550.98 541.91c-0.99-28.904-4.377-57.939-9.421-86.393-6.111-34.469-13.889-85.002-43.983-107.46-17.404-12.988-39.941-17.249-59.865-25.081-9.697-3.81-18.384-7.594-26.537-11.901-27.518 30.176-63.4 45.962-105.19 45.964-41.774 0-77.652-15.786-105.17-45.964-8.153 4.308-16.84 8.093-26.537 11.901-19.924 7.832-42.461 12.092-59.863 25.081-30.096 22.463-37.873 72.996-43.983 107.46-5.045 28.454-8.433 57.489-9.422 86.393-0.766 22.387 10.288 25.525 29.017 32.284 23.453 8.458 47.666 14.737 72.041 19.884 47.077 9.941 95.603 17.582 143.92 17.924 48.318-0.343 96.844-7.983 143.92-17.924 24.375-5.145 48.59-11.424 72.041-19.884 18.736-6.757 29.789-9.895 29.023-32.284z"/>
                    </g>
                    </g>
                  </g>
                  </svg>
                  </div>
                  <strong class="number">Sunil Kumar</strong>
                  <span>Really a great experience. We can increase speed of typing and also earn some money</span>
                </div>
              </div>
            </div>
            <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate fadeInUp ftco-animated">
              <div class="block-18 text-center">
								<div class="text">
                  <div class="icon">
                    <svg version="1.1" viewBox="0 0 250 250" xmlns="http://www.w3.org/2000/svg">
                    <g transform="translate(-266.43 -401.65)">
                      <g transform="matrix(.68333 0 0 .68333 581.94 550.77)" fill="#95bbdf">
                      <rect x="-461.73" y="-218.23" width="365.85" height="365.85" rx="0" ry="0" color="#000000" fill="#95bbdf"/>
                      </g>
                      <g transform="matrix(.3492 0 0 .3492 284.57 419.79)" fill="#FFF">
                      <g fill="#FFF">
                        <path d="m306 325.99c90.56-0.01 123.15-90.68 131.68-165.17 10.51-91.76-32.88-160.82-131.68-160.82-98.78 0-142.19 69.055-131.68 160.82 8.54 74.484 41.114 165.18 131.68 165.17z"/>
                        <path d="m550.98 541.91c-0.99-28.904-4.377-57.939-9.421-86.393-6.111-34.469-13.889-85.002-43.983-107.46-17.404-12.988-39.941-17.249-59.865-25.081-9.697-3.81-18.384-7.594-26.537-11.901-27.518 30.176-63.4 45.962-105.19 45.964-41.774 0-77.652-15.786-105.17-45.964-8.153 4.308-16.84 8.093-26.537 11.901-19.924 7.832-42.461 12.092-59.863 25.081-30.096 22.463-37.873 72.996-43.983 107.46-5.045 28.454-8.433 57.489-9.422 86.393-0.766 22.387 10.288 25.525 29.017 32.284 23.453 8.458 47.666 14.737 72.041 19.884 47.077 9.941 95.603 17.582 143.92 17.924 48.318-0.343 96.844-7.983 143.92-17.924 24.375-5.145 48.59-11.424 72.041-19.884 18.736-6.757 29.789-9.895 29.023-32.284z"/>
                      </g>
                      </g>
                    </g>
                    </svg>
                  </div>
                  <strong class="number">Rinku</strong>
                  <span>Really loved this site. Learn and earn together</span>
                </div>
              </div>
            </div>
						<div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate fadeInUp ftco-animated">
              <div class="block-18 text-center">
								<div class="text">
                  <div class="icon">
                  <svg version="1.1" viewBox="0 0 250 250" xmlns="http://www.w3.org/2000/svg">
                  <g transform="translate(-266.43 -401.65)">
                    <g transform="matrix(.68333 0 0 .68333 581.94 550.77)" fill="#95bbdf">
                    <rect x="-461.73" y="-218.23" width="365.85" height="365.85" rx="0" ry="0" color="#000000" fill="#95bbdf"/>
                    </g>
                    <g transform="matrix(.3492 0 0 .3492 284.57 419.79)" fill="#FFF">
                    <g fill="#FFF">
                      <path d="m306 325.99c90.56-0.01 123.15-90.68 131.68-165.17 10.51-91.76-32.88-160.82-131.68-160.82-98.78 0-142.19 69.055-131.68 160.82 8.54 74.484 41.114 165.18 131.68 165.17z"/>
                      <path d="m550.98 541.91c-0.99-28.904-4.377-57.939-9.421-86.393-6.111-34.469-13.889-85.002-43.983-107.46-17.404-12.988-39.941-17.249-59.865-25.081-9.697-3.81-18.384-7.594-26.537-11.901-27.518 30.176-63.4 45.962-105.19 45.964-41.774 0-77.652-15.786-105.17-45.964-8.153 4.308-16.84 8.093-26.537 11.901-19.924 7.832-42.461 12.092-59.863 25.081-30.096 22.463-37.873 72.996-43.983 107.46-5.045 28.454-8.433 57.489-9.422 86.393-0.766 22.387 10.288 25.525 29.017 32.284 23.453 8.458 47.666 14.737 72.041 19.884 47.077 9.941 95.603 17.582 143.92 17.924 48.318-0.343 96.844-7.983 143.92-17.924 24.375-5.145 48.59-11.424 72.041-19.884 18.736-6.757 29.789-9.895 29.023-32.284z"/>
                    </g>
                    </g>
                  </g>
                  </svg>
                  </div>
                  <strong class="number">Sushil</strong>
                  <span>Really a great idea to learn typing and increse speed of typing</span>
                </div>
              </div>
            </div>
						<div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate fadeInUp ftco-animated">
              <div class="block-18 text-center">
								<div class="text">
                  <div class="icon">
                  <svg version="1.1" viewBox="0 0 250 250" xmlns="http://www.w3.org/2000/svg">
                  <g transform="translate(-266.43 -401.65)">
                    <g transform="matrix(.68333 0 0 .68333 581.94 550.77)" fill="#95bbdf">
                    <rect x="-461.73" y="-218.23" width="365.85" height="365.85" rx="0" ry="0" color="#000000" fill="#95bbdf"/>
                    </g>
                    <g transform="matrix(.3492 0 0 .3492 284.57 419.79)" fill="#FFF">
                    <g fill="#FFF">
                      <path d="m306 325.99c90.56-0.01 123.15-90.68 131.68-165.17 10.51-91.76-32.88-160.82-131.68-160.82-98.78 0-142.19 69.055-131.68 160.82 8.54 74.484 41.114 165.18 131.68 165.17z"/>
                      <path d="m550.98 541.91c-0.99-28.904-4.377-57.939-9.421-86.393-6.111-34.469-13.889-85.002-43.983-107.46-17.404-12.988-39.941-17.249-59.865-25.081-9.697-3.81-18.384-7.594-26.537-11.901-27.518 30.176-63.4 45.962-105.19 45.964-41.774 0-77.652-15.786-105.17-45.964-8.153 4.308-16.84 8.093-26.537 11.901-19.924 7.832-42.461 12.092-59.863 25.081-30.096 22.463-37.873 72.996-43.983 107.46-5.045 28.454-8.433 57.489-9.422 86.393-0.766 22.387 10.288 25.525 29.017 32.284 23.453 8.458 47.666 14.737 72.041 19.884 47.077 9.941 95.603 17.582 143.92 17.924 48.318-0.343 96.844-7.983 143.92-17.924 24.375-5.145 48.59-11.424 72.041-19.884 18.736-6.757 29.789-9.895 29.023-32.284z"/>
                    </g>
                    </g>
                  </g>
                  </svg>
                  </div>
                  <strong class="number">Manish</strong>
                  <span>Paricipated in Two contest and stand first</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @include('elements.common.footer')
  <script src="{{asset('assets/dist/js/time.js')}}"></script>
  <script src="{!! asset('assets/dist/js/ajax-call.js') !!}"></script>
  <script>
    $(".DateCountdown").TimeCircles({
      use_background: false,
      circle_bg_color: "#000000",
      time: {
        Days: { color: "#ffffff" },
        Hours: { color: "#ffffff" },
        Minutes: { color: "#ffffff" },
        Seconds: { color: "#ffffff" }
      }
    });
  </script>
</body>
</html>
