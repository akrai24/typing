@extends('layouts.admin')

@section('title', 'Edit Contest')

@section('pageHeader', 'Edit Contest')

@section('content')
    <div class="container-fluid">

        {!! Form::model($contest, ['route' => ['contest.update', $contest->id], 'method' => 'patch']) !!}

            @include('admin/contest/_form', ['disable' => true] )
            <div class="row pb-5">
                <div class="col-md-12">
                    {!! Form::reset('Cancel', ['class' => 'btn btn-secondary']) !!}
                    {!! Form::submit('Create New Contest', ['class' => 'btn btn-success float-right']) !!}
                </div>
            </div>
        {!! Form::close() !!}

    </div><!-- /.container-fluid -->
@endsection
