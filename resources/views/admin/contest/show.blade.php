@extends('layouts.admin')

@section('title', $contest->title.' Details')

@section('pageHeader', $contest->title.' Details')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Contest Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="descriptions" class="form-label">Contest Description</label>
                                    <div contenteditable readonly class="form-control" id="descriptions" >{{$contest->description}}</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="start_time" class="control-label">Start Time</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <input class="form-control" readonly type="text"  value="{{$contest->start_time}}" id="start_time">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="end_time" class="control-label">End Time</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <input class="form-control" readonly type="text" value="{{$contest->end_time}}" id="end_time">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="typing_text" class="form-label">Contest Typing Text</label>
                                    <div contenteditable readonly class="form-control" id="typing_text">{{$contest->typing_text}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Prize Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="total_prize" class="control-label">Prizing Amount</label>
                                <input class="form-control" readonly type="text" value="{{$contest->total_prize}}" id="total_prize">
                            </div>
                            <div class="form-group col">
                                <label for="entry_fees" class="control-label">Entry Fees</label>
                                <input class="form-control" readonly type="text" value="{{$contest->entry_fees}}" id="entry_fees">
                            </div>
                            <div class="form-group col">
                                <label for="max_users" class="control-label">Max Users</label>
                                <input class="form-control" readonly type="text" value="{{$contest->max_users}}" id="max_users">
                            </div>
                            <div class="form-group col">
                                <label for="max_users" class="control-label">Registered Users</label>
                                <input class="form-control" readonly type="text" value="{{$contest->reg_users}}" id="max_users">
                            </div>
                        </div>
                        <h5 class="mt-4 mb-3">Prize Breakup</h5>
                        @foreach ($contest->prizings as $prizing)
                            <div class="form-row">
                                <div class="col form-group">
                                    <label for="from[]" class="control-label">Rank From</label>
                                    <input class="form-control" readonly type="text" value="{{$prizing->rank_from}}" id="from[]">
                                </div>
                                <div class="col form-group">
                                    <label for="to[]" class="control-label">Rank To</label>
                                    <input class="form-control" readonly type="text" value="{{$prizing->rank_to}}" id="to[]">
                                </div>
                                <div class="col form-group">
                                    <label for="prize[]" class="control-label">Prizing Amount</label>
                                    <input class="form-control" readonly type="text" value="{{$prizing->amount}}" id="prize[]">
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <!-- /.card-body -->
                </div>

                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center justify-content-between">
                            <div class="col-6">
                                <h3 class="card-title">Contest Status & Result</h3>
                            </div>
                            <div class="col-6  text-right">
                                @if($contest->result == 'in-review')
                                    <a href="{!! route('users.updateBalance', $contest->id) !!}" class="btn btn-success" role="button">Publish Results</a>
                                @elseif($contest->result == 'completed')
                                    <a href="" class="btn btn-success disabled" role="button" disabled>Contest Finished</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @if ($contest->start_time > \Carbon\Carbon::now())
                            <div class="alert alert-info" role="alert">
                                Contest not started yet!
                            </div>
                        @elseif($contest->start_time > \Carbon\Carbon::now() && $contest->end_time < \Carbon\Carbon::now())
                            <div class="alert alert-success" role="alert">
                                Contest in process!
                            </div>
                        @elseif($contest->end_time < \Carbon\Carbon::now())
                            @if($contest->reg_users != $contest->max_users)
                                <div class="alert alert-danger" role="alert">
                                    Not enough contestants to start the contest!
                                </div>
                            @else
                                <table class="table table-light">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">Users</th>
                                            <th scope="col">Points</th>
                                            <th scope="col">Rank</th>
                                            <th scope="col">Prize</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($contest->typingTests as $ranking)
                                            <tr>
                                                <td>{!! $ranking->user->name !!}</td>
                                                <td>{!! $ranking->points !!}</td>
                                                <td>{!! $ranking->rank !!}</td>
                                                <td>&#8377; {!! $ranking->amount !!}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
@endsection
