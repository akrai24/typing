@extends('layouts.admin')

@section('title', 'Contests List')

@section('pageHeader', 'All Contest')

@section('content')
    <div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Contests</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table class="table table-bordered">
              <thead class="thead-light">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Title</th>
                  <th scope="col">Entry Fees</th>
                  <th scope="col">Max Users</th>
                  <th scope="col">Total Prize</th>
                  <th scope="col">Start time</th>
                  <th scope="col">End Time</th>
                  <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($contests as $key => $contest)
                    <tr>
                        <th scope="row">{{$key + $contests->firstItem()}}</th>
                        <td>
                            <a href="{{route('contest.show',$contest->id)}}">{{$contest->title}}  @php echo  ($contest->result == 'in-review') ? ' <i class="fa fa-clock-o fa-lg"></i>' : '';  @endphp</a>
                        </td>
                        <td>{{$contest->entry_fees}}</td>
                        <td>{{$contest->max_users}}</td>
                        <td>{{$contest->total_prize}}</td>
                        <td>{{$contest->start_time}}</td>
                        <td>{{$contest->end_time}}</td>
                        <td><a href="{{route('contest.edit',$contest->id)}}"><i class="fa fa-edit"></i></a></td>
                    </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            {!! $contests->links() !!}
        </div>
      </div>
    </div>
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
@endsection
