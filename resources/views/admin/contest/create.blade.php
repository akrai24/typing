@extends('layouts.admin')

@section('title', 'New Contest')

@section('pageHeader', 'Create New Contest')

@section('content')
    <div class="container-fluid">
        @include('common.errors')
        {!! Form::open(['route' => 'contest.store', 'method' => 'post']) !!}
            @include('admin/contest/_form',['disable' => false])
        <div class="row pb-5">
            <div class="col-md-12">
                {!! Form::reset('Cancel', ['class' => 'btn btn-secondary']) !!}
                {!! Form::submit('Create New Contest', ['class' => 'btn btn-success float-right']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div><!-- /.container-fluid -->
@endsection
