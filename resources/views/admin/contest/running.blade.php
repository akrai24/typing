@extends('layouts.admin')

@section('title', 'Contests List')

@section('pageHeader', 'All Contest')

@section('content')
    <div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Contests</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table class="table table-bordered">
            <tr>
              <th style="width: 10px">#</th>
              <th>Title</th>
              <th>Entry Fees</th>
              <th>Max Users</th>
              <th>Total Prize</th>
              <th>Start time</th>
              <th>End Time</th>
              <th>Actions</th>
            </tr>
            @foreach ($contests as $key => $contest)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$contest->title}}</td>
                    <td>{{$contest->entry_fees}}</td>
                    <td>{{$contest->max_users}}</td>
                    <td>{{$contest->total_prize}}</td>
                    <td>{{$contest->start_time}}</td>
                    <td>{{$contest->end_time}}</td>
                    <td><a href="{{route('contest.edit',$contest->id)}}"><i class="fa fa-edit"></i></a></td>
                </tr>
            @endforeach
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
@endsection
