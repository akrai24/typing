@extends('layouts.admin')
@section('title', 'Dashboard')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-3 col-6">
			<div class="small-box bg-info">
				<div class="inner">
					<h3>{{$counts['contests']}}</h3>

					<p>Total Contests</p>
				</div>
				<div class="icon">
					<i class="ion ion-bag"></i>
				</div>
				<a href="{{route('contest.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-lg-3 col-6">
			<div class="small-box bg-success">
				<div class="inner">
					<h3>{{$counts['running']}}</h3>

					<p>Running Contest</p>
				</div>
				<div class="icon">
					<i class="ion ion-stats-bars"></i>
				</div>
				<a href="{{route('contest.running')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-lg-3 col-6">
			<div class="small-box bg-warning">
				<div class="inner">
					<h3>{{$counts['users']}}</h3>

					<p>User Registrations</p>
				</div>
				<div class="icon">
					<i class="ion ion-person-add"></i>
				</div>
				<a href="{{route('admin.user.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>

		<div class="col-lg-3 col-6">

			<div class="small-box bg-danger">
				<div class="inner">
					<h3>{{$counts['blocked']}}</h3>

					<p>Blocked Users</p>
				</div>
				<div class="icon">
					<i class="fa fa-user-times" aria-hidden="true"></i>
				</div>
				<a href="{{route('users.block')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>

	</div>

</div>
@endsection
