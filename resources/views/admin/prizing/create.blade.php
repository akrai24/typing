@extends('layouts.admin')
@section('content')
    <?php $contest = Session::get('contest'); ?>
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Prize for {{$contest->title}}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="{{route('prizing.store', $contest->id)}}">
                        <div class="card-body">
                            @csrf
                            <input type="hidden" name="contest_id" value="{{ $contest->id }}">
                            <div class=" input-box">
                                <div class="row input-row">
                                    <div class="form-group col-md-4">
                                        <label for="From">From</label>
                                        <input type="number" class="form-control" id="from" name="from[]"
                                               placeholder="Enter From number">
                                        @error('from')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="To">To</label>
                                        <input type="number" class="form-control" id="to" name="to[]"
                                               placeholder="Enter To number">
                                        @error('to')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="Prize">Prize</label>
                                        <input type="number" class="form-control" id="prize" name="prize[]"
                                               placeholder="Enter Prize">
                                        @error('prize')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <i class="btn btn-success clone-input fa fa-plus pull-right"></i>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
