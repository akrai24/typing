<div class="col-xm-6 col-md-6 mb-3">
    <div class="home-match-card4 px-4">
        <div class="row text-muted text-muted home-prize" data-toggle="modal" data-target="#leader-board" data-id="{!! $contest->id !!}" data-url="{!! route('contestDetails', $contest->id) !!}">
            <div class="col-12 text-left">
                <h5 class="text-capitalize contest-title">
                    <a href="#;">{{$contest->title}}</a>
                </h5>
            </div>
        </div>
        <div class="row justify-content-center align-items-center">
            <div class="col-sm-3 col-6 text-left order-sm-1" data-toggle="modal" data-target="#leader-board" data-id="{!! $contest->id !!}" data-url="{!! route('contestDetails', $contest->id) !!}">
                <small class="text-muted mb-1">Total Prize</small>
                <h5 class="m-0"><i class="fas fa-rupee-sign"></i> {{$contest->total_prize}}</h5>
            </div>
            <div class="col-sm-6 text-center order-sm-2 order-12">
                @if ($contest->start_time > \Carbon\Carbon::now())
                    <div class="DateCountdown" data-date="{{$contest->start_time}}"></div>
                @elseif ( ($contest->start_time < \Carbon\Carbon::now()) && ($contest->end_time > \Carbon\Carbon::now()) )
                    <a href="{{route('typing', $contest->id)}}" class="btn btn-outline-success">Dive-in Contest</a>
                @endif
            </div>
            <div class="col-sm-3 col-6 text-right order-sm-3">
                <small class="text-muted mb-1">Entry</small>
                <form action="{{route('order',$contest->id)}}" method="post">
                    <input type="hidden" name="fees" value="{{$contest->entry_fees}}">
                    @if ((Auth::check() &&  $contest->hasUser(Auth::user()->id)) || ($contest->reg_users == $contest->max_users ))
                        <button type="submit" class="btn btn-success join" disabled><i class="fas fa-rupee-sign"></i> {{$contest->entry_fees}}</button>
                    @else
                        <button type="submit" class="btn btn-success join"><i class="fas fa-rupee-sign"></i> {{$contest->entry_fees}}</button>
                    @endif
                </form>
            </div>
        </div>
        <div class="progress mt-3" style="height:8px">
            <div class="progress-bar bg-gold" style="width:{{ ($contest->reg_users / $contest->max_users) * 100  }}%"></div>
        </div>
        <div class="row">
            <div class="col-6 text-left text-gold">
                <p>{{($contest->max_users - $contest->reg_users)}} spot left</p>
            </div>
            <div class="col-6 text-right	text-muted text-muted home-prize">
                <p>{{$contest->max_users}} spots</p>
            </div>
        </div>
    </div>
</div>