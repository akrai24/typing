@extends('layouts.main')
@section('content')
    @include('contest._leader-board-modal')
    <div class="upcoming-matches">
        <div class="row">
            <div class="col-md-12">
                <h3 class="upcoming-match-title">UPCOMING MATCHES</h3>
            </div>
        </div>
        <div class="row">
            @foreach ($contests as $key => $contest)
                @include('contest.contest-grid')
            @endforeach
        </div>
    </div>
@endsection
