@extends('layouts.main')
@section('content')
<div class="upcoming-matches">
  <div class="row">
      <div class="col-md-12">
        <h3 class="upcoming-match-title">{{$title}}</h3>
    </div>
  </div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  <div class="row">
    
    <div class="col-md-6">
        <form action="{{route('contact-mail')}}" method="post">
            @csrf
            <div class="form-group">
                <label for=""><strong>Name(*)</strong></label>
                <input type="text" class="form-control" name="name" require>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for=""><strong>Email(*)</strong></label>
                <input type="text" class="form-control" name="email" require>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for=""><strong>Subject(*)</strong></label>
                <input type="text" class="form-control" name="subject">
                @error('subject')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for=""><strong>Message(*)</strong></label>
                <textarea name="message" class="form-control" rows="5"></textarea require>
                @error('message')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                @captcha
            </div>
            <div class="form-group">
            
            <label for="">Enter Captcha</label>
            <input type="text" id="captcha" name="captcha" class="form-control" require>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection
