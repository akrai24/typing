<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    function __construct() {

    }

    private $API_KEY = '286945AHpxz0Xz5d3b43ad';
    private $SENDER_ID = "Typing";
    private $RESPONSE_TYPE = 'json';

    public function sendSMS($otp, $mobileNumber){
        
        
        $curl = curl_init();
        $message = urlencode("Your verification code is ".$otp);

        $url = "https://control.msg91.com/api/sendotp.php?otp=".$otp."&sender=".$this->SENDER_ID."&message=".$message."&mobile=".$mobileNumber."&authkey=".$this->API_KEY;
        // echo  $url;exit;
        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
        return $response;
        }

    }
}
