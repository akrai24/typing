<?php

namespace App\Http\Middleware;

use Closure;
use App\Contest;
use Illuminate\Support\Facades\Auth;

class JoinedContest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!empty($request->contest) && !empty(auth()->user()->id)){
            $contest = Contest::find($request->contest);
            // echo date('Y-m-d H:i:s');exit;
            if(\Carbon\Carbon::now() > $contest->start_time){
                if (\Carbon\Carbon::now() < $contest->end_time){
                    if($contest->hasUser(Auth::id())){
                        if ($contest->reg_users == $contest->max_users){
                            return $next($request);
                        } else {
                            return redirect()->back()->with('error', 'Contest have not completed the required users.');
                        }
                    }else{
                        return redirect()->back()->with('error', 'You have not joined this contest');
                    }
                } else {
                    return redirect()->back()->with('error', 'Contest not started yet.');
                }
            }else{
                return redirect()->back()->with('error', 'Contest already finished');
            }
        }else{
            return redirect()->back()->with('error', 'Something went wrong. Click again on the contest or try again later');
        }
        
    }
}
