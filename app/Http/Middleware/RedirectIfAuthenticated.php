<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = User::where('id',Auth::id())->exists();
        if($user){
            return redirect('/home');
        }
        if (Auth::guard('admin')->check()) {
            return redirect('/admin/home');
        }
        return $next($request);
    }
}
