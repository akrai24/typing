<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $contest = \App\Contest::all();
        count($contest);exit;
        return view('home',compact('contests'));
    }


    /**
     * Show the Welcome Page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function welcome()
    {
        $contests = \App\Contest::where('start_time', '>', \Carbon\Carbon::now())->where('status', '1')->get();
        $contestCount = \App\Contest::count();
        $contestActive = \App\Contest::where('start_time', '<', \Carbon\Carbon::now())
                                ->where('end_time', '>', \Carbon\Carbon::now())
                                ->where('status', '1')
                                ->count();
        $usersCount = \App\User::where('is_block', '0')->count();
        return view('contest.upcoming-contest-list', compact('contests', 'usersCount', 'contestCount', 'contestActive'));
    }

    public function contestDetails($id)
    {
        $contest = \App\Contest::find($id);
        $rankings = \App\TypingTest::where('contest_id', $id)->orderBy('points','DESC')->get();
        return view('modals.contest-details', compact('contest', 'rankings'));
    }

    public function aboutUs()
    {
        $title = "About Us";
        $contests = \App\Contest::where('start_time', '>', \Carbon\Carbon::now())->where('status', '1')->get();
        $contestCount = \App\Contest::count();
        $contestActive = \App\Contest::where('start_time', '<', \Carbon\Carbon::now())
            ->where('end_time', '>', \Carbon\Carbon::now())
            ->where('status', '1')
            ->count();
        $usersCount = \App\User::where('is_block', '0')->count();
        return view('about_us', compact('contests', 'usersCount', 'contestCount', 'title','contestActive'));
    }

    public function contactUs()
    {
        $title = "Contact Us";
        $contests = \App\Contest::where('start_time', '>', \Carbon\Carbon::now())->where('status', '1')->get();
        $contestCount = \App\Contest::count();
        $contestActive = \App\Contest::where('start_time', '<', \Carbon\Carbon::now())
            ->where('end_time', '>', \Carbon\Carbon::now())
            ->where('status', '1')
            ->count();
        $usersCount = \App\User::where('is_block', '0')->count();
        return view('contect_us', compact('contests', 'usersCount', 'contestCount', 'title','contestActive'));
    }

    public function faq()
    {
        $title = "FAQ";
        $contests = \App\Contest::where('start_time', '>', \Carbon\Carbon::now())->where('status', '1')->get();
        $contestCount = \App\Contest::count();
        $contestActive = \App\Contest::where('start_time', '<', \Carbon\Carbon::now())
            ->where('end_time', '>', \Carbon\Carbon::now())
            ->where('status', '1')
            ->count();
        $usersCount = \App\User::where('is_block', '0')->count();
        return view('faq', compact('contests', 'usersCount', 'contestCount', 'title','contestActive'));
    }

    public function terms()
    {
        $title = "Terms & Conditions";
        $contests = \App\Contest::where('start_time', '>', \Carbon\Carbon::now())->where('status', '1')->get();
        $contestCount = \App\Contest::count();
        $usersCount = \App\User::where('is_block', '0')->count();
        return view('terms', compact('contests', 'usersCount', 'contestCount', 'title'));
    }

    public function contactmail(Request $request){

        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'captcha' => 'required|captcha',
        ]);

        $contactUs = new \App\Contact;
        $contactUs->name = $request->name;
        $contactUs->email = $request->email;
        $contactUs->subject = $request->subject;
        $contactUs->message = $request->message;

        $contactUs->save();

        return redirect()->back()->with('success', 'Admin will contact you shortly');
    }

}
