<?php

namespace App\Http\Controllers;

use App\Prizing;
use Illuminate\Http\Request;

class PrizingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.prizing.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isMethod('post')){
            $contest_id = $request->route('contest');
            for ($i = 0; $i < count($request->from); $i++){
                $prize = new Prizing();
                $prize->contest_id = $contest_id;
                $prize->rank_from = $request->from[$i];
                $prize->rank_to = (isset($request->to[$i]) ? $request->to[$i] : '');
                $prize->amount = $request->prize[$i];
                $prize->save();
            }
            return redirect(route('contest.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prizing  $prizing
     * @return \Illuminate\Http\Response
     */
    public function show(Prizing $prizing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prizing  $prizing
     * @return \Illuminate\Http\Response
     */
    public function edit(Prizing $prizing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prizing  $prizing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prizing $prizing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prizing  $prizing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prizing $prizing)
    {
        //
    }
}
