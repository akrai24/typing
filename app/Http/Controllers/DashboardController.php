<?php

namespace App\Http\Controllers;

use App\Contest;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $counts = \DB::select("SELECT (SELECT COUNT(*) FROM contests) as contests, (SELECT COUNT(*) FROM users) as users, (SELECT COUNT(*) FROM contests WHERE start_time < '".\Carbon\Carbon::now()."') as running, (SELECT COUNT(*) FROM users WHERE is_block = '1') as blocked");
        $counts['contests'] = Contest::all()->count();
        $counts['running'] = Contest::where('start_time', '<', \Carbon\Carbon::now())->where('end_time', '>', \Carbon\Carbon::now())->get()->count();
        $counts['users'] = User::all()->count();
        $counts['blocked'] = User::where('is_block',1)->get()->count();
        return view('admin.dashboard.index', compact('counts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
