<?php

namespace App\Http\Controllers;

use App\Contest;
use App\Redeem;
use App\TypingTest;
use Illuminate\Http\Request;
use App\User;
use App\Rules\OtpVerify;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usersList = User::where('is_block',0)->paginate(15);
        return view('admin.users.index', compact('usersList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'My Profile';
        $user = User::find($id);
        return view('users.profile', compact('user','title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $title = 'Edit Profile';
        return view('users.edit', compact('user','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->update($request->all());
        return redirect()->route('user.show', $user->id)->with('success','Profile updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function blockUsers(){
        $blockedUser = User::where('is_block', 1)->paginate(20);

        return view('admin.users.block', compact('blockedUser'));
    }

    public function unBlockUsers($id){
        $blockedUser = User::where('id', $id)->first();
        $blockedUser->is_block = !$blockedUser->is_block;
        $blockedUser->save();
        return back()->with('message', 'Operation Successfully Completed');
    }

    public function updateBalance($id){
        $results = TypingTest::where('contest_id',$id)->get();
        foreach ($results as $result) :
            $user = User::find($result->user_id);
            $user->balance = $user->balance + $result->amount;
            $user->save();
        endforeach;
        $contest = Contest::find($id);
        $contest->result = 'completed';
        $contest->save();
        return back()->with('success','Result published successfully');
    }

    public function profile(){
        $title = 'My Profile';
        $user = User::find(\Auth::id());
        return view('users.profile', compact('user','title'));
    }

    public function myContests()
    {
        $title = 'My Contests';
        $user = User::find(\Auth::id());
        $contests = $user->contests()->get();
        $contestActive = $user->contests()->where('start_time', '<', \Carbon\Carbon::now())
            ->where('end_time', '>', \Carbon\Carbon::now())
            ->where('status', '1')
            ->get();
        $expired = $user->contests()->where('end_time', '<', \Carbon\Carbon::now())
            ->where('status', '1')
            ->get();
        return view('users.contests', compact('title','user', 'contests','contestActive','expired'));
    }

    public function myEarning($id)
    {
        $title = 'My Earnings';
        $user = User::find($id);
        $redeems = Redeem::where('user_id',$id)->get();
        $redeem_request = Redeem::where(['user_id' => $id, 'status' => 0])->get();
        $redeemed = Redeem::where(['user_id' => $id, 'status' => 1])->get();
        return view('users.earning', compact('title','user', 'redeems','redeem_request','redeemed'));
    }

    public function verify(Request $request)
    {
        return $request->user()->hasVerified()
            ? redirect()->route('home')
            : view('verify-otp');
    }

    public function verifyOTP(Request $request){
        $request->validate([
            'otp' => 'required',
            'otp' => [new OtpVerify],
        ]);

        if ($request->user()->hasVerified()) {
            return redirect()->route('home');
        }

        $request->user()->markAsVerified();

        return redirect()->route('user.show', Auth::user()->id)->with('success', 'Your account successfully verified!');
    }

    public function contestResult(Contest $contest){
        $title = 'Contest Result';
        $rankings = \App\TypingTest::where('contest_id', $contest->id)->orderBy('points','DESC')->get();
        $contest->result = 'in-review';
        $contest->save();
        return view('users.result', compact('contest','title','rankings'));
    }

}
