<?php

namespace App\Console\Commands;

use App\Contest;
use App\User;
use Illuminate\Console\Command;

class CancelContestCorn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cancelcontest:corn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel contest if user limit not fulfilled. ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info('Corn is Working Fine!');

        $contests = Contest::where('start_time', '<', \Carbon\Carbon::now())
                            ->where('status', '1')->get();

        foreach ($contests as $contest) :
            if ($contest->reg_users != $contest->max_users) :
                $contest->status = 0;
                $contest->result = 'canceled';
                $contest->save();

                // Refund Balance to users
                foreach ($contest->users as $user) :
                    $contest_user = User::find($user->id);
                    $contest_user->balance = ($contest_user->balance + $contest->entry_fees);
                    $contest_user->save();
                endforeach;

            endif;
        endforeach;

        $this->info('Demo:Cron Command Run successfully!');

    }
}
